# HRnet Application

A React web application to manage employee records of the company Wealth Health.

## Informations

This application is made entirely with React, has two pages: the employee creation page and the employee list page,
with featuring react components

## Prerequisites

- [Node.js (v16.13.1)](https://nodejs.org/en/)
- [Recommended IDE (Visual Studio Code)](https://code.visualstudio.com)

## Installation

First of all, clone the project with HTTPS

```bash
  git clone https://gitlab.com/bailombs/mamadoubailosow_13_27052022.git
```

Need to be inside the root of the projeet Folder, and run these commands (install dependencies, and run locally).

```

yarn install and
yarn start
```

## Dependencies

| Name             | Version |
| ---------------- | ------- |
| react            | 18.1.0  |
| react-dom        | 18.1.0  |
| react-scripts    | 5.0.1   |
| react-icons      | 4.4.0   |
| react-redux      | 2.1.8   |
| redux toolkit    | 1.8.3   |
| react-router-dom | 6.3.0   |
