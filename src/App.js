import React from 'react';
import { Route, Routes } from 'react-router-dom';
import CreateEmployee from './pages/CreateEmployee';
import EmployeeList from './pages/EmployeeList';
import './styles/index.scss';

const App = () => {
    return (
        <Routes>
            <Route path='/' element={<CreateEmployee />} />
            <Route path='/employee' element={<EmployeeList />} />
        </Routes>
    );
};

export default App;