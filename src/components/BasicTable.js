import React, { useMemo } from 'react';
import { MdOutlineArrowDropDown, MdOutlineArrowDropUp } from 'react-icons/md';
import { useSelector } from 'react-redux';
import { useGlobalFilter, usePagination, useSortBy, useTable } from "react-table";
import { COLUMNS } from '../data/columns';
import { rowsRange } from '../data/data';
import InputFilter from './InputFilter';
import Pagination from './Pagination';
import SelectRows from './SelectRows';


const BasicTable = () => {

    const employees_redux = useSelector((state) => state.employees)


    const columns = useMemo(() => COLUMNS, [])
    const data = useMemo(() => employees_redux, [employees_redux])


    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow,
        setGlobalFilter,
        page,
        nextPage,
        previousPage,
        canNextPage,
        canPreviousPage,
        pageOptions,
        setPageSize,
        state,
        gotoPage,
        preGlobalFilteredRows,
        globalFilteredRows

    } = useTable({
        columns: columns,
        data: data,
    }, useGlobalFilter, useSortBy, usePagination)

    const { pageIndex, pageSize, globalFilter } = state

    return (
        <main className='employee-list'>
            <section className='employee-list__search'>
                <SelectRows
                    data={rowsRange}
                    setPageSize={setPageSize}
                />
                <InputFilter filter={globalFilter} setFilter={setGlobalFilter} />
            </section>

            <table {...getTableProps()}>
                <thead>
                    {headerGroups.map(headerGroup => (
                        <tr {...headerGroup.getHeaderGroupProps()}>
                            {headerGroup.headers.map(column => (
                                <th {...column.getHeaderProps(column.getSortByToggleProps())}>
                                    {column.render('Header')}
                                    <span>
                                        {column.isSorted ? (column.isSortedDesc ? <MdOutlineArrowDropDown className='arrow' /> : <MdOutlineArrowDropUp className='arrow' />) : ''}
                                    </span>
                                </th>
                            ))}
                        </tr>
                    ))}
                </thead>
                <tbody {...getTableBodyProps()}>
                    {page.map((row, i) => {
                        prepareRow(row)
                        return (
                            <tr {...row.getRowProps()}>
                                {row.cells.map(cell => {
                                    return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                                })}
                            </tr>
                        )
                    })}
                </tbody>
            </table>

            <Pagination
                pageOptions={pageOptions}
                rows={rows}
                pageIndex={pageIndex}
                gotoPage={gotoPage}
                pageSize={pageSize}
                previousPage={previousPage}
                nextPage={nextPage}
                canPreviousPage={canPreviousPage}
                canNextPage={canNextPage}
                preGlobalfilteredRows={preGlobalFilteredRows}
                globalFilteredRows={globalFilteredRows}
            />
        </main>
    );
};

export default BasicTable;