import React from 'react';
import Select from 'react-dropdown-select';
import { RiArrowDownSFill, RiArrowUpSFill } from 'react-icons/ri';
import { departments, statesData } from '../data/data';

const DropDownMenu = ({ label, onChange, name }) => {

    const dataChoice = label === 'Departement' ? departments : statesData


    return (
        <React.Fragment>
            <label htmlFor={label}>{label}</label>
            <Select
                id={label}
                options={dataChoice}
                values={[]}
                valueField={label === 'Departement' ? 'departement' : 'name'}
                labelField={label === 'Departement' ? 'departement' : 'name'}
                keepSelectedInList={false}
                onChange={onChange}
                className='bgColor'
                dropdownHeight='150px'
                name={name}
                dropdownHandleRenderer={({ state }) => (
                    state.dropdown ? <RiArrowUpSFill /> : <RiArrowDownSFill />
                )}
            />
        </React.Fragment>
    );
};

export default DropDownMenu;



/**
 * Input PROPTYPES
 */
// DropDown.propTypes = {
//     data: PropTypes.Array,
//     register: PropTypes.string,
//     errors: PropTypes.object
// };