import { yupResolver } from '@hookform/resolvers/yup';
import { Modal, useModal } from 'modal-react-sow';
import React from 'react';
import { Controller, useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import * as yup from "yup";
import { addEmployee } from '../redux/slice';
import DropDownMenu from './DropDownMenu';
import LabelInputText from './LabelInputText';




const FormContainer = () => {
    const { isOpen, toggle } = useModal()
    const dispacth = useDispatch()

    const schema = yup.object().shape({
        firstname: yup.string().required('First Name is required'),
        lastname: yup.string().required('Last Name is required'),
        dateofbirth: yup.date().required('Date of Birth is required'),
        startdate: yup.date().required('Start Date is required'),
        street: yup.string().required('Street is required'),
        city: yup.string().required('City is required'),
        state: yup.array().required('State is required'),
        zipcode: yup.number('must be a number').positive().integer().required('Zip code is required'),
        departement: yup.array().required('Departement is required')
    }).required();


    const { register, handleSubmit, formState, control, reset, resetField } = useForm({
        resolver: yupResolver(schema),
    });

    const { errors, isValid, isSubmitted, isSubmitSuccessful, isDirty } = formState

    const dateFormater = (date) => {
        return date.toLocaleDateString("en-US")
    }

    const onSubmit = async (data) => {
        console.log(data)

        const startdate = dateFormater(data?.startdate)
        const dateofbirth = dateFormater(data?.dateofbirth)

        const employees = JSON.parse(localStorage.getItem('employees')) || []
        const employee = {
            firstname: data?.firstname,
            lastname: data?.lastname,
            dateofbirth: dateofbirth,
            startdate: startdate,
            departement: data?.departement[0]?.departement,
            street: data?.street,
            city: data?.city,
            state: data?.state[0]?.abbreviation,
            zipcode: data?.zipcode
        }
        employees.push(employee)
        dispacth(addEmployee(employee))
        localStorage.setItem("employees", JSON.stringify(employees))

        reset()
        resetField('departement')
    }
    console.log(errors, 'valid', isValid, 'submitted', isSubmitted, 'succes', isSubmitSuccessful, isDirty)

    return (
        <main className='main'>
            <h2 className='main__create-employee'>Create Employee</h2>
            <form onSubmit={handleSubmit(onSubmit)} className='form'>
                <LabelInputText label='First Name' register={register} typeInput='text' />
                <span className='form__error-msg'>{errors.firstname?.message}</span>

                <LabelInputText label='Last Name' register={register} typeInput='text' />
                <span className='form__error-msg'>{errors.lastname?.message}</span>

                <LabelInputText label='Date of Birth' register={register} typeInput='date' />
                <span className='form__error-msg'>{errors.dateofbirth?.message.slice(0, 33)}</span>

                <LabelInputText label='Start Date' register={register} typeInput='date' />
                <span className='form__error-msg'>{errors.startdate?.message.slice(0, 31)}</span>

                <fieldset className='form__fieldset'>
                    <legend>Address</legend>
                    <LabelInputText
                        label='Street'
                        register={register}
                    />
                    {errors.street && <span className='form__error-msg'>{errors.street.message}</span>}

                    <LabelInputText label='City' register={register} />
                    <span className='form__error-msg'>{errors.city?.message}</span>

                    <Controller
                        name="state"
                        control={control}
                        render={({ field: { onChange, name } }) =>
                        (<DropDownMenu
                            label='State'
                            onChange={onChange}
                            name={name}
                        />)}
                    />
                    <span className='form__error-msg'>{errors.state?.message.slice(0, 30)}</span>

                    <LabelInputText label='Zip Code' register={register} typeInput='number' />
                    <span className='form__error-msg'>{errors.zipcode?.message.slice(0, 31)}</span>

                </fieldset>

                <Controller
                    name="departement"
                    control={control}
                    render={({ field: { onChange, name } }) =>
                    (<DropDownMenu
                        label='Departement'
                        onChange={onChange}
                        name={name}
                        className='state-dropdown'
                    />)}
                />
                <span className='form__error-msg'>{errors.departement?.message}</span>

                <input type="submit" value='Save' className='form__submit' onClick={() => { toggle() }} />
                {isSubmitSuccessful && <Modal isOpen={isOpen} toggle={toggle} modalMessage='Employee Created' />}
            </form>
        </main>
    );
};

export default FormContainer;