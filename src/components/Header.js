import React from 'react';
import { NavLink } from 'react-router-dom';

const Header = ({ page }) => {
    return (
        <header className='header'>
            <h1 className='header__title'>HRnet</h1>
            <div className='header__link'>
                <NavLink to={page ? '/employee' : '/'} className='header__link--link'>
                    {
                        page === 'home' ? 'View Curent Employee' : 'Home'
                    }
                </NavLink>
            </div>
        </header>
    );
};

export default Header;