import React, { useState } from 'react';
import { AiOutlineSearch } from 'react-icons/ai';
import { useDispatch } from 'react-redux';
import { getCurrentFilteredlength } from '../redux/slice';

const InputFilter = ({ setFilter }) => {
    const [filterLength, setFilterLength] = useState(0)
    const dispatch = useDispatch()

    dispatch(getCurrentFilteredlength(filterLength))

    return (
        <aside className='employee-list__search--wrapper'>
            <AiOutlineSearch className='search-icon' />
            <input
                className='search-input'
                type='search'
                onChange={(e) => {
                    setFilter(e.target.value)
                    setFilterLength(e.target.value.length)
                }}
                placeholder='search'

            />
        </aside>
    );
};

export default InputFilter;
