import React from 'react';

const LabelInputText = ({ label, register, typeInput, required }) => {

    // format le label pour relier le label et l'input et and the name
    const labelFormat = label.toLowerCase().split(' ').join('')

    return (
        <div className='form__inputwrapper'>
            <label htmlFor={labelFormat}>{label}</label>
            <input
                type={typeInput}
                id={labelFormat}
                {...register(labelFormat, { required })}
                placeholder={label}
            />
        </div>
    );
};

export default LabelInputText;