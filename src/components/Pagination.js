import React, { useState } from 'react';
import { useSelector } from 'react-redux';

const Pagination = ({
    pageOptions,
    rows,
    pageIndex,
    gotoPage,
    pageSize,
    previousPage,
    nextPage,
    canPreviousPage,
    canNextPage,
    preGlobalfilteredRows,
    globalFilteredRows

}) => {

    const [currentOption, setCurrentOption] = useState(0)
    const curentFilteredLength = useSelector((state) => state.currentFilteredlength)
    const firstRowOptions = currentOption + 1
    const secondRowOption = rows.length < (firstRowOptions * pageSize) ? rows.length : firstRowOptions * pageSize

    return (
        <section className='employee-list__pagination'>
            <div className='employee-list__pagination--showing'>
                <span>
                    Showing
                    <strong>
                        {firstRowOptions}  to {secondRowOption} of {rows.length} entries
                    </strong>
                    {(curentFilteredLength > 0) && `  ( filtered from ${preGlobalfilteredRows.length} total entries )`}
                </span>
            </div>
            <div className='employee-list__pagination--prev-next'>
                <div className='jump'>
                    <label htmlFor="goto" className='jump__label'>Jump to page</label>


                    <input
                        type='number'
                        onChange={(e) => {
                            const pageNumber = e.target.value ? (e.target.value - 1) : 0
                            console.log(pageNumber);
                            gotoPage(pageNumber)
                            setCurrentOption(pageSize * pageNumber)
                        }}
                        placeholder='N°'
                        min={1}
                        max={pageOptions.length}
                        id='goto'
                        className='jump__goto'
                    />
                    <input type='button'
                        id='goto'
                        className='jump__input'
                        value={pageIndex + 1}
                    />
                </div>
                <button
                    onClick={() => {
                        previousPage();
                        setCurrentOption(currentOption - pageSize)
                        gotoPage(pageIndex - 1)

                    }}
                    disabled={!canPreviousPage}
                    style={{ backgroundColor: (!canPreviousPage ? '#2e8b57' : '') }}
                >
                    Previous
                </button>

                <button
                    onClick={() => {
                        nextPage();
                        setCurrentOption(pageSize + currentOption)
                        gotoPage(pageIndex + 1)
                    }}
                    disabled={!canNextPage}
                    style={{ backgroundColor: (!canNextPage ? '#2e8b57' : '') }}
                >
                    Next
                </button>
            </div>

        </section>
    );
};

export default Pagination;