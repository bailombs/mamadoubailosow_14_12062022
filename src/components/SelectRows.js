import React from 'react';
import Select from 'react-dropdown-select';
import { RiArrowDownSFill, RiArrowUpSFill } from 'react-icons/ri';

const SelectRows = ({ data, setPageSize, pageSize }) => {


    return (
        <aside className='employee-list__search--select'>
            Show
            <Select
                className='select-row'
                placeholder='select'
                options={data}
                values={[]}
                valueField='range'
                labelField='range'
                keepSelectedInList={false}
                multi={false}
                onChange={(values) => {
                    setPageSize(values[0].range);
                }}
                searchable={false}
                dropdownHandleRenderer={({ state }) => (
                    state.dropdown ? <RiArrowUpSFill /> : <RiArrowDownSFill />
                )}
            />
            Entries
        </aside>
    );
};

export default SelectRows;