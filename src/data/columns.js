export const COLUMNS = [
    {
        Header: 'First Name',
        accessor: 'firstname'
    },
    {
        Header: 'Last Name',
        accessor: 'lastname'
    },
    {
        Header: 'Start Date',
        accessor: 'startdate'
    },
    {
        Header: 'Department',
        accessor: 'departement'
    },
    {
        Header: 'Birth Date',
        accessor: 'dateofbirth'
    },
    {
        Header: 'Street',
        accessor: 'street'
    },
    {
        Header: 'City',
        accessor: 'city'
    },
    {
        Header: 'State',
        accessor: 'state'
    },
    {
        Header: 'Zip Code',
        accessor: 'zipcode'
    },
];
