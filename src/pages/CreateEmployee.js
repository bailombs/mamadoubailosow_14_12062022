import React from 'react';
import FormContainer from '../components/FormContainer';
import Header from '../components/Header';

const CreateEmployee = () => {
    return (
        <React.Fragment>
            <Header page='home' />
            <FormContainer />
        </React.Fragment>
    );
};

export default CreateEmployee;