import React from 'react';
import BasicTable from '../components/BasicTable';
import Header from '../components/Header';

const EmployeeList = () => {

    return (
        <React.Fragment >
            <Header />
            <h1 className='title'>Current Employees</h1>
            <BasicTable />
        </React.Fragment>
    );
};

export default EmployeeList;