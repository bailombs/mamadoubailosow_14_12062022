import { createSlice } from "@reduxjs/toolkit";
import { EMPLOYEES } from "../data/data";

export const slice = createSlice({
    name: 'employees-list',
    initialState: {
        currentFilteredlength: 0,
        employees: EMPLOYEES,
    },
    reducers: {
        getCurrentFilteredlength: (state, action) => {
            state.currentFilteredlength = action.payload
        },
        addEmployee: (state, action) => {
            state.employees.push(action.payload)
        }
    }
})
export default slice.reducer;
export const { getCurrentFilteredlength, addEmployee } = slice.actions