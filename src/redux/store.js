import { configureStore } from "@reduxjs/toolkit";
import filteredLengthReducer from './slice.js';

export default configureStore({
    reducer: filteredLengthReducer
})